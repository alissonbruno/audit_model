module AuditModel
  module Controllers
    module Helpers
      extend ActiveSupport::Concern
      included do
        send :include, InstanceMethods
        before_filter :set_current_user
        after_action :clear_current_user
      end

      module InstanceMethods
        def set_current_user
          Thread.current[:user] = get_user
        end

        def clear_current_user
          Thread.current[:user] = nil
        end

        private
        def get_user
          send(Config.user_method) if respond_to? Config.user_method 
        end
      end
    end
  end
end