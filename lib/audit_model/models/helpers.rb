module AuditModel
  module Models
    module Helpers
      extend ActiveSupport::Concern

      module ClassMethods
        def auditable(options = {})
          class_attribute(:audit_enabled,   instance_writer: false)
          class_attribute(:true_condition,  instance_writer: false)
          class_attribute(:false_condition, instance_writer: false) 
          
          options[:on] ||= [:create, :update, :destroy]
          setup_callbacks(options[:on])
          setup_model()
          setup_associations()
          
          send(:include, InstanceMethods)
          
          setup_conditional(options)
          
          send(:enable_audit)
          
          
        end

        def without_audit 
          cache_enable = audit_enabled
          disable_audit
          yield
        ensure
          enable_audit if cache_enable
        end

        def disable_audit
          self.audit_enabled = false
        end

        def enable_audit
          self.audit_enabled = true
        end

        private
        def setup_callbacks(options_on = [])
          options_on.each do |option|
            send "callback_on_#{option}"
          end
        end
        
        def setup_conditional(options)
          options = options.dup
          if options.key?(:if)
            self.true_condition = options[:if]
          elsif options.key?(:unless)
            self.false_condition = options[:unless] 
          end
        end

        def setup_model
          Object.const_set audit_class, Class.new(Models::Base)
        end

        def setup_associations
          has_many :revisions, as: :model, class_name: "AuditModel::Models::Revision"
          has_many :audits, through: :revisions, source: :audit, source_type: audit_class
        end

        def callback_on_create
          after_create :audit_on_create
        end

        def callback_on_update
          before_update :audit_on_update
        end

        def callback_on_destroy
          after_destroy :audit_on_destroy
        end

        def audit_class 
          "#{name}Audit"
        end
      end

      module InstanceMethods
        private
          def is_enabled?
            if(true_condition && self.respond_to?(true_condition))
              send(true_condition)
            elsif(false_condition && self.respond_to?(false_condition))
              !send(false_condition)
            end
          end

          def audit_on_create
            Revision.audit(self, :create) if is_enabled? && self.class.audit_enabled 
          end

          def audit_on_update
            Revision.audit(self, :update) if self.changed? && is_enabled? && self.class.audit_enabled
          end

          def audit_on_destroy
            Revision.audit(self, :destroy) unless self.new_record? || !is_enabled? || !self.class.audit_enabled
          end
        
      end
    end
  end
end