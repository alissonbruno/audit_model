module AuditModel
  module Models
    class Base < ActiveRecord::Base
      self.abstract_class = true
      has_one :revision, foreign_key: "audit_id"

      BLACK_LIST = [
        "id", 
        "created_at", 
        "updated_at"
      ]

      def who
        revision.user
      end

      class << self
        def build(model, type)
          return if type == :update && !any_change?(model)
          attrs = sanitize(model, type)
          attrs[:rev_type] = type
          new(attrs)
        end

        private

        def sanitize(model, type)
          if type == :update
            attrs = model.attributes.merge(model.changed_attributes)
          elsif 
            attrs = model.attributes
          end
          attrs.except(*BLACK_LIST).select { |key,value| has_audit?(key) }
        end

        def any_change?(model)
          model.changed? && model.changed_attributes.any? { |key,value| has_audit?(key) }
        end

        def has_audit?(attribute)
          attribute_names.include?(attribute)
        end
      end
    end
  end
end
