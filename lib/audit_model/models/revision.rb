module AuditModel
  module Models
    class Revision < ActiveRecord::Base
      belongs_to :model, polymorphic: true
      belongs_to :audit, polymorphic: true
      belongs_to :user, polymorphic: true

      class << self
        def audit(model, type) 
          data = {
            model: model,
            user: Thread.current[:user],
            revision_date: Time.now,
            audit: audit_class_for(model).build(model, type)
          }
          create(data)
        end

        private

        def audit_class_for(model) 
          "#{model.class}Audit".constantize
        end
      end
    end
  end
end