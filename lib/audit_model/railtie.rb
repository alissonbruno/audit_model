module AuditModel
  class Railtie < Rails::Railtie
    config.audit_model = Config
    initializer "audit_model.controllers.include_helpers" do
      ActiveSupport.on_load(:action_controller) do
        include Controllers::Helpers
      end
    end

    initializer "audit_model.models.include_helpers" do
      ActiveSupport.on_load(:active_record) do
        include Models::Helpers
      end
    end
  end
end