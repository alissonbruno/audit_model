class CreateRevisions < ActiveRecord::Migration
  create_table :revisions do |t|
    t.integer   :user_id
    t.string    :user_type
    t.integer   :model_id
    t.string    :model_type
    t.integer   :audit_id
    t.string    :audit_type

    t.timestamp :revision_date
  end

  add_index :revisions, [:user_id, :user_type], name: 'revision_user_index'
  add_index :revisions, [:model_id, :model_type], name: 'revision_model_index'
  add_index :revisions, [:audit_id, :audit_type], name: 'revision_audit_index'
end