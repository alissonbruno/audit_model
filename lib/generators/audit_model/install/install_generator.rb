require 'rails/generators/active_record'
module AuditModel
  module Generators
    class InstallGenerator < Rails::Generators::Base
      include Rails::Generators::Migration
      source_root File.expand_path("../templates", __FILE__)

      def copy_initializer
        copy_file "audit_model.rb", "config/initializers/audit_model.rb"
      end
      def copy_migration
        migration_template "revision.rb", "db/migrate/create_revisions.rb"
      end

      def self.next_migration_number(dirname)
        ActiveRecord::Generators::Base.next_migration_number(dirname)
      end
    end
  end
end