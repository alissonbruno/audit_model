require 'rails/generators/active_record'
require 'generators/audit_model/column_not_found_error'
module AuditModel
  module Generators
    class AuditGenerator < ActiveRecord::Generators::Base
      source_root File.expand_path("../templates", __FILE__)
      argument :properties, type: :array, default: []

      def klass
        begin
          class_name.constantize
        rescue
          raise "#{class_name} not defined"
        end
      end

      def valid_properties?
        properties.each do |propertie|
          raise AuditModel::ColumnNotFoundError.new(klass: class_name, column: propertie) unless klass.attribute_names.include? propertie 
        end
      end

      def create_audit_migration
        migration_template "audit_migration.rb", "db/migrate/create_#{table_name}.rb"
      end

      def audited_properties
        klass.columns.select {|column| properties.include? column.name }
      end

      def table_name
        "#{class_name}_audits".downcase
      end

      def migration_class_name
        "Create#{table_name.camelize}"
      end
    end
  end
end