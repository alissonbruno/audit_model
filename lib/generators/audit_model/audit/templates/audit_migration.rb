class <%= migration_class_name %> < ActiveRecord::Migration
  def change
    create_table(:<%= table_name %>) do |t|
<% audited_properties.each do |column| -%>
      t.<%= column.type %> :<%= column.name %>
<% end -%>
      t.string :rev_type
    end
  end
end