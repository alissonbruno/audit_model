module AuditModel
  class ColumnNotFoundError < StandardError
    def initialize(info)
      super("The column #{info[:column]} doesn't exist in #{info[:klass]}!")
    end
  end
end