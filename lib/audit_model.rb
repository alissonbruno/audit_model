require 'active_support'
require 'active_support/rails'
require "audit_model/version"

module AuditModel
  extend ActiveSupport::Autoload

  autoload :Config, 'audit_model/config'

  module Controllers
    autoload :Helpers, 'audit_model/controllers/helpers'
  end

  module Models
    autoload :Base, 'audit_model/models/base'
    autoload :Revision, 'audit_model/models/revision'
    autoload :Helpers, 'audit_model/models/helpers'
  end

  class << self
    def setup
      yield Config
    end
  end
end

require "audit_model/railtie" if defined?(Rails::Railtie)