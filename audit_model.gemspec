# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'audit_model/version'

Gem::Specification.new do |spec|
  spec.name          = "audit_model"
  spec.platform      = Gem::Platform::RUBY
  spec.version       = AuditModel::VERSION
  spec.summary       = %q{Audit changes to your models.}
  spec.description   = %q{With gem audit_model you can log all changes to your models.}
  
  spec.required_ruby_version     = '>= 2.2.2'
  spec.license       = 'MIT'

  spec.authors       = ["Alisson Bruno"]
  spec.email         = ["alissonbruno.sa@gmail.com"]
  spec.homepage      = "https://bitbucket.org/alissonbruno/audit_model"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"

  spec.add_dependency 'activerecord', [">=4.2", "< 5.1"]
end
