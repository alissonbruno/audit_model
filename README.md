# AuditModel

Track changes to your models, for auditing or versioning.

## Installation

Add audit_model to your application's Gemfile

```
#!ruby
gem 'audit_model'
```

And then execute

```
rails g audit_model:install
rake db:migrate
```
For each model you want to audit you should run this command:
```
rails g audit_model:audit MODEL field1 field2 ...
rake db:migrate
```
Where MODEL is your model what you want to do audit and (field1, field2) are the fields that you want to be audited. 
Add auditable to the models you want to track.

```
#!ruby
class User < ActiveRecord::Base
  auditable
end
```

## Configuration
When you run the following command you create a configuration file **config/initializers/audit_model.rb**
```
rails g audit_model:install
```
You can configure which method the application uses to retrieve the current user by default the method is [#current_user](#).
```
#!ruby
AuditModel.setup do |config|
  config.user_method = "current_user"  
end
```

## Usage
In the model you have [#audits](#) method that returns the audits.
```
#!ruby
user = User.create(email: "user@example.com", name: "user example")
user.audits
```
Audits are ActiveRecord models, knowing that you can make any query based on ActiveRecord.
You can do something like this:
```
#!ruby
user.audits.last
```
This will return the last change in the model.
You can also use [#revisions](#)
```
#!ruby
user = User.find 3
revision = user.revisions.last
```
So it's possible to obtain information
```
#!ruby
revision.user # User who made the changes
revision.revision_date 
revision.model # Model that was audited.
revision.audit # Model with the audit information
```
## Disabling auditing

If you want to disable auditing, you can use [#without_audit](#) in your model class.

```
#!ruby
User.without_audit do 
  User.create(name: "User example", email: "user.example@domain.com")
end

User.without_audit do 
  user = User.find 3
  user.update(email: "user-example@domain.com")
end
```

## License

AuditModel is released under the [MIT License](http://www.opensource.org/licenses/MIT).


## Inspirations

* [Audited](https://github.com/collectiveidea/audited)
* [PaperTrail](https://github.com/airblade/paper_trail)


## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/alissonbruno/audit_model